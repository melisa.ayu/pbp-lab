from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
    input_attrs = {
        'type':'text',
        'placeholder':'YYYY-MM-DD'
    }
    date_of_birth = forms.DateField(widget=forms.DateInput(attrs=input_attrs))