##### 1. Apakah perbedaan antara JSON dan XML?
JSON (_JavaScript Object Notation_) dan XML (_Extensible Markup Language_) memang tampak memiliki tujuan yang sama. Namun, keduanya sebenarnya berbeda. Perbedaan tersebut akan dijelaskan pada tabel berikut.

| JSON | XML |
|------|-----|
|JSON adalah format data yang ditulis dalam JavaScript| XML adalah bahasa markup, bukan bahasa pemrograman|
|Data pada JSON disimpan seperti map yang berisi pasangan key dan value| Data XML disimpan sebagai struktur tree|
|JSON tidak melakukan pemrosesan atau komputasi apapun| XML dapat menjalankan pemrosesan dan formatting pada dokumen maupun objek|
|Transmisi data lebih cepat| Transmisi data lebih lambat|
|Tidak ada ketentuan untuk namespaces, menambahkan komentar, atau menuliskan metadata| Mendukung namespaces, komentar, dan metadata|
|Ukuran dokumennya ringkas, sederhana, dan mudah dibaca| Ukuran dokumen besar dan strukturnya terlihat lebih kompleks untuk dibaca|
|Mendukung penggunaan dan pengaksesan array| Tidak mendukung penggunaan array secara langsung|
|Hanya mendukung tipe data string, number, boolean, dan objek yang hanya dapat mengandung tipe data primitif| Mendukung banyak tipe data yang kompleks termasuk charts, images, dan tipe data nonprimitif lainnya|
|Mendukung pengkodean UTF dan ASCII| Mendukung pengkodean UTF-8 dan UTF-16|

##### 2. Apakah perbedaan antara HTML dan XML?
Perbedaan HTML (_Hyper Text Markup Language_) dan XML (_Extensible Markup Language_) dapat dijelaskan pada tabel sebagai berikut.
| HTML | XML |
|------|-----|
|HTML adalah bahasa markup| XML adalah bahasa markup standar yang menyediakan framework untuk mendefinisikan bahasa markup lainnya|
|Berfungsi untuk menampilkan data| Berfungsi untuk menyimpan dan mentransfer data|
|Bersifat static| Bersifat dynamic|
|Dapat mengabaikan small errors| Tidak memperbolehkan adanya error|
|Tidak case sensitive| Case sensitive|
|Tag pada HTML merupakan predefined tag| Tag pada XML merupakan user defined tag|
|Memiliki batasan jumlah tag| Tag bersifat extensible|
|Tidak mempertahankan white space| Dapat mempertahankan white space|
|Tag HTML berfungsi untuk menampilkan data| Tag XML berfungsi untuk describing atau menggambarkan data|
|HTML tidak membawa data, hanya menampilkannya| XML membawa data dari dan menuju database|

##### Daftar Pustaka
Shankar, R. (2021, January 7). _JSON vs XML in 2021: Comparison, Features & Example_. Hackr.Io. https://hackr.io/blog/json-vs-xml <br>
GeeksforGeeks. (2021, August 25). _HTML vs XML_. https://www.geeksforgeeks.org/html-vs-xml/