from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30)
    sender = models.CharField(max_length=30, name='from')
    title = models.CharField(max_length=100)
    message = models.TextField()