import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Note App',
      theme: ThemeData.dark(),
      home: FormPage(),
    );
  }
}


/**
 * MODEL CLASS
 */
class Note {
  final String from;
  final String to;
  final String title;
  final String message;

  const Note({
    required this.from,
    required this.to,
    required this.title,
    required this.message,
  });
}


/**
 * FORM PAGE (HOME)
 */
class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  static List<Note> listNote = [];
  var _fromController = new TextEditingController();
  var _toController = new TextEditingController();
  var _titleController = new TextEditingController();
  var _messageController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text("Add Note"),
      ),
      body: ListView(
        padding: EdgeInsets.all(50),
        children: <Widget>[
          buildTextField(_fromController, 'From'),
          buildTextField(_toController, 'To'),
          buildTextField(_titleController, 'Title'),
          buildTextField(_messageController, 'Message'),
          ListTile(
            title: RaisedButton(
              color: Colors.indigoAccent,
              child: Text("Submit"),
              onPressed: () {
                var route = MaterialPageRoute(builder: (BuildContext context) {
                  addNote(_fromController.text, _toController.text,
                      _titleController.text, _messageController.text);
                  return CardPage(
                    listNote: listNote,
                  );
                });
                Navigator.of(context).push(route);
              },
            ),
          ),
        ],
      ),
    );
  }

  ListTile buildTextField(TextEditingController textController, String label) {
    return ListTile(
      title: TextField(
        controller: textController,
        decoration: InputDecoration(labelText: label),
      ),
    );
  }

  void addNote(String from, String to, String title, String message) {
    listNote.add(new Note(from: from, to: to, title: title, message: message));
  }
}


/**
 * NOTE CARD PAGE
 * display created notes as cards
 */
class CardPage extends StatefulWidget {
  final List<Note> listNote;

  CardPage({Key? key, required this.listNote}) : super(key: key);

  @override
  _CardPageState createState() => _CardPageState();
}

class _CardPageState extends State<CardPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: Text("Note Card"),
      ),
      body: Container(
        margin: EdgeInsets.all(50),
        child: ListView(
          children: <Widget>[
            if (widget.listNote.isEmpty)
              Center(child: Text('Belum ada catatan'))
            else
              for (Note item in widget.listNote)
                buildCard(item.from, item.to, item.title, item.message)
          ],
        ),
      ),
    );
  }

  Card buildCard(String from, String to, String title, String message) {
    return Card(
      elevation: 5,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: 10),
          ListTile(
            title: Text('From: ' + from + '\nTo: ' + to),
          ),
          ListTile(
            title: Text(title),
            subtitle: Text(message),
          ),
          const SizedBox(height: 10),
        ],
      ),
    );
  }
}


/**
 * MENU DRAWER
 */
class MainDrawer extends StatelessWidget {
  final padding = EdgeInsets.all(20);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        child: ListView(
          padding: padding,
          children: <Widget>[
            const SizedBox(height: 48),
            Text(
              'NOTE',
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 30),
            ),
            const SizedBox(height: 48),
            buildMenuItem(
              text: 'Add Note',
              icon: Icons.note_add_outlined,
              onClicked: () => selectedItem(context, 0),
            ),
            const SizedBox(height: 16),
            buildMenuItem(
              text: 'Cards',
              icon: Icons.sticky_note_2_outlined,
              onClicked: () => selectedItem(context, 1),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildMenuItem({
    required String text,
    required IconData icon,
    VoidCallback? onClicked,
  }) {
    final color = Colors.white;
    final hoverColor = Colors.black12;

    return ListTile(
      leading: Icon(icon, color: color),
      title: Text(text, style: TextStyle(color: color)),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  void selectedItem(BuildContext context, int i) {
    switch (i) {
      case 0:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => FormPage()));
        break;
      case 1:
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) =>
                CardPage(listNote: _FormPageState.listNote)));
        break;
    }
  }
}
