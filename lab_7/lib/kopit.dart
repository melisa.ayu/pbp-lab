import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Kopit",
    theme: ThemeData(
      primarySwatch: Colors.blueGrey,
      canvasColor: Color.fromRGBO(79, 189, 194, 1),
    ),
    home: DestinationForm(),
  ));
}

class DestinationForm extends StatefulWidget {
  @override
  _DestinationFormState createState() => _DestinationFormState();
}

class _DestinationFormState extends State<DestinationForm> {
  final _formKey = GlobalKey<FormState>();

  bool bukaCheckBox = false;

  var _provinsiController = new TextEditingController();
  var _kotaController = new TextEditingController();
  var _namaController = new TextEditingController();
  var _alamatController = new TextEditingController();
  var _jamBukaController = new TextEditingController();
  var _jamTutupController = new TextEditingController();
  var _bukaLagiController = new TextEditingController();
  var _catatanController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Formulir Destinasi"),
        backgroundColor: Color.fromRGBO(10, 60, 83, 1),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _provinsiController,
                    decoration: new InputDecoration(
                      labelText: "Provinsi",
                      icon: Icon(Icons.travel_explore_outlined),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Provinsi tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _kotaController,
                    decoration: new InputDecoration(
                      labelText: "Kabupaten/Kota",
                      icon: Icon(Icons.location_city_outlined),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Kabupaten/Kota tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _namaController,
                    decoration: new InputDecoration(
                      labelText: "Nama Destinasi",
                      icon: Icon(Icons.beach_access_rounded),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama destinasi tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _alamatController,
                    decoration: new InputDecoration(
                      labelText: "Alamat Destinasi",
                      icon: Icon(Icons.add_location_alt_outlined),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Alamat destinasi tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _jamBukaController,
                    decoration: new InputDecoration(
                      hintText: "Contoh 09:00",
                      labelText: "Jam Buka",
                      icon: Icon(Icons.access_time_outlined),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Jam buka tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _jamTutupController,
                    decoration: new InputDecoration(
                      hintText: "Contoh 21:00",
                      labelText: "Jam Tutup",
                      icon: Icon(Icons.access_time_filled),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Jam tutup tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                CheckboxListTile(
                  title: Text('Destinasi ini ditutup sementara karena COVID-19'),
                  value: bukaCheckBox,
                  activeColor: Color.fromRGBO(5, 55, 55, 1),
                  onChanged: (value) {
                    setState(() {
                      bukaCheckBox = value!;
                    });
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _bukaLagiController,
                    decoration: new InputDecoration(
                      hintText: "Tahun-Bulan-Tanggal",
                      labelText: "Tanggal Destinasi Dibuka Kembali",
                      icon: Icon(Icons.date_range_rounded),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return null;
                      }
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: _catatanController,
                    decoration: new InputDecoration(
                      labelText: "Catatan",
                      icon: Icon(Icons.sticky_note_2_outlined),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return null;
                      }
                    },
                  ),
                ),
                ElevatedButton(
                  child: Text(
                    "Kirim",
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: Color.fromRGBO(5, 55, 55, 0.7),
                    onPrimary: Colors.white,
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      print(_kotaController.text + ", " + _provinsiController.text);
                      print(_namaController.text);
                      print(_alamatController.text);
                      if (bukaCheckBox == true) print("TUTUP");
                      print("Buka pukul " + _jamBukaController.text + " - " + _jamTutupController.text);
                      if (_bukaLagiController.text != "") print("Buka kembali pada " + _bukaLagiController.text);
                      if (_catatanController.text != "") print(_catatanController.text);
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}